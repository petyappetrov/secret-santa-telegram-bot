# Stage 1 - the build process
FROM node:latest as build-deps
WORKDIR /usr/local/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . ./
RUN npm run build

# Stage 2 - the production environment
FROM node
WORKDIR /usr/local/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install --production
COPY --from=build-deps /usr/local/app/dist ./dist
EXPOSE 8080
CMD npm run start:prod
