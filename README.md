
# Телеграм бот "Тайный-Санта" @secret_santa_claus_bot

## Описание:
Самый простой способ организации игры "Тайный-Санта" в своих группах.
Для случайного распределения участников применяется алгоритм Фишера-Йетса.
Основная процедура тасования Фишера-Йетса аналогична случайному вытаскиванию записок с числами из шляпы одна записка за другим, пока записки не кончатся.

## Инструкция:
1. Добавить бота в группу
2. Вызвать команду /happy_new_year
3. Дождаться всех участников
4. Вызвать команду /ho_ho_ho
5. Встретить новый год с близкими 😌


# Для разработчиков:
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```


db.revokeRolesFromUser(
    "reportsUser",
    [
      { role: "readWrite", db: "accounts" }
    ]
)

db.createUser(
    {
      user: "admin-santa",
      pwd: passwordPrompt(),
      roles: [
         { role: "readWrite", db: "santa" }
      ]
    }
)
