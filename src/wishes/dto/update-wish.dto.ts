import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateWishDTO {
  @IsNotEmpty()
  @IsString()
  text: string;
}
