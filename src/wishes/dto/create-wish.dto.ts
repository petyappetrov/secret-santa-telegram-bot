import { IsMongoId, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Types } from 'mongoose';

export class CreateWishDTO {
  @IsNotEmpty()
  @IsMongoId()
  user: Types.ObjectId;

  @IsNotEmpty()
  @IsMongoId()
  @IsNumber()
  room: Types.ObjectId;

  @IsNotEmpty()
  @IsString()
  text: string;
}
