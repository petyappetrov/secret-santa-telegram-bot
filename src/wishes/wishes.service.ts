import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { CreateWishDTO } from './dto/create-wish.dto';
import { UpdateWishDTO } from './dto/update-wish.dto';
import { Wish } from './schemas/wish.schema';

@Injectable()
export class WishesService {
  constructor(
    @InjectModel(Wish.name) private readonly wishModel: Model<Wish>,
  ) {}

  public async createWish(createWishDTO: CreateWishDTO): Promise<Wish> {
    return this.wishModel.create(createWishDTO);
  }

  public async updateWish(
    wishId: Types.ObjectId,
    updateWishDTO: UpdateWishDTO,
  ): Promise<Wish> {
    return this.wishModel
      .findByIdAndUpdate(wishId, updateWishDTO, { new: true })
      .exec();
  }

  public async getWish(
    user: Types.ObjectId,
    room: Types.ObjectId,
  ): Promise<Wish> {
    return this.wishModel.findOne({ user, room });
  }
}
