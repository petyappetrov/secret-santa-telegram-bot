import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Room } from '@rooms/schemas/room.schema';
import { User } from '@users/schemas/user.schema';
import { Document, Types } from 'mongoose';

@Schema({
  versionKey: false,
  timestamps: true,
})
export class Wish extends Document {
  @Prop({
    type: Types.ObjectId,
    ref: User,
  })
  user: Types.ObjectId;

  @Prop({
    type: Types.ObjectId,
    ref: Room,
  })
  room: Types.ObjectId;

  @Prop({
    required: true,
  })
  text: string;
}

export const WishSchema = SchemaFactory.createForClass(Wish);
