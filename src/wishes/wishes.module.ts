import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Wish, WishSchema } from './schemas/wish.schema';
import { WishesService } from './wishes.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Wish.name, schema: WishSchema }]),
  ],
  providers: [WishesService],
  exports: [WishesService],
})
export class WishesModule {}
