import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RoomsModule } from '@rooms/rooms.module';
import { UsersModule } from '@users/users.module';
import { WishesModule } from '@wishes/wishes.module';
import { TelegrafModule } from 'nestjs-telegraf';
import { UtilsService } from 'src/utils.service';

import { TelegramBotActions } from './telegram-bot.actions';
import { TelegramBotCommands } from './telegram-bot.commands';

@Module({
  imports: [
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        token: configService.get('TELEGRAM_BOT_TOKEN'),
      }),
    }),
    RoomsModule,
    UsersModule,
    WishesModule,
  ],
  providers: [TelegramBotCommands, TelegramBotActions, UtilsService],
})
export class TelegramBotModule {}
