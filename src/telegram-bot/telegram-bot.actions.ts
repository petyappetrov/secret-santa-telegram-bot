import { Injectable } from '@nestjs/common';
import { UsersService } from '@users/users.service';
import { WishesService } from '@wishes/wishes.service';
import { Types } from 'mongoose';
import {
  Action,
  BaseScene,
  Context,
  InjectBot,
  session,
  Stage,
  TelegrafProvider,
} from 'nestjs-telegraf';

@Injectable()
export class TelegramBotActions {
  constructor(
    @InjectBot() private bot: TelegrafProvider,
    private readonly usersService: UsersService,
    private readonly wishesService: WishesService,
  ) {
    this.init();
  }

  async init() {
    const scene = await this.wishScene();
    const stage = new Stage([scene]);
    stage.command('cancel', Stage.leave());
    this.bot.use(session());
    this.bot.use(stage.middleware());
  }

  private async wishScene() {
    const scene = new BaseScene('wish');
    scene.enter(async (ctx: Context) => {
      if (!ctx?.session?.sceneWishRoomId) {
        try {
          await ctx.reply('Ошибка...');
        } catch (error) {}
        return ctx.scene.leave();
      }

      try {
        await ctx.reply('Напиши свое пожелание к подарку');
      } catch (error) {}
    });

    scene.on('message', async (ctx: Context) => {
      const user = await this.usersService.getUserByTelegram(ctx.from.id);
      const existWish = await this.wishesService.getWish(
        user._id,
        ctx?.session?.sceneWishRoomId,
      );

      if (existWish) {
        await this.wishesService.updateWish(existWish._id, {
          text: ctx.message.text,
        });
        ctx.reply('Вишлист успешно изменён 👍');
      } else {
        await this.wishesService.createWish({
          user: user._id,
          room: ctx?.session?.sceneWishRoomId,
          text: ctx.message.text,
        });
        ctx.reply('Вишлист успешно добавлен 👍');
      }

      ctx.session.sceneWishRoomId = undefined;
      ctx.state.wishAdded = true;
      ctx.scene.leave();
    });

    scene.leave((ctx: Context) => {
      if (!ctx.state.wishAdded) {
        ctx.state.wishRoomId = undefined;
        ctx.reply('Вы вышли из редактора пожелания');
      }
    });
    return scene;
  }

  @Action(/wish_room/)
  actionWishRoom(ctx: Context) {
    ctx.session.sceneWishRoomId = Types.ObjectId(
      ctx.callbackQuery.data.split('~')[1],
    );
    ctx.scene.enter('wish');
  }
}
