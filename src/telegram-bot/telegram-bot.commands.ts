import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RoomsService } from '@rooms/rooms.service';
import { UsersService } from '@users/users.service';
import { WishesService } from '@wishes/wishes.service';
import { Command, Context, InjectBot, TelegrafProvider } from 'nestjs-telegraf';
import { UtilsService } from 'src/utils.service';
import { Chat } from 'telegraf/typings/telegram-types';

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

@Injectable()
export class TelegramBotCommands implements OnModuleInit {
  constructor(
    @InjectBot() private bot: TelegrafProvider,
    private readonly roomsService: RoomsService,
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
    private readonly wishesService: WishesService,
    private readonly utilsService: UtilsService,
  ) {}

  async onModuleInit() {
    this.bot.use(async (ctx, next) => {
      try {
        if (ctx.chat.type === 'channel') {
          return;
        }
        if (ctx.botInfo.is_bot && ctx?.session && !ctx?.session?.bot) {
          const member = await ctx.getChatMember(ctx.botInfo.id);
          ctx.session.bot = {
            isAdministrator: member.status === 'administrator',
          };
        }

        const chat: Chat & { permissions?: any } = await ctx.getChat();
        if (
          !chat.permissions.can_send_messages ||
          !chat.permissions.can_send_media_messages ||
          !chat.permissions.can_pin_messages
        ) {
          return;
        }

        return next();
      } catch (error) {
        next();
      }
    });

    this.bot.catch((err: Error, ctx: Context) => {
      const message = `Error message: ${err.message}`;
      console.log(message);
      return ctx.telegram.sendMessage(
        this.configService.get('DEVELOPER_TELEGRAM_ID'),
        message,
      );
    });
  }

  @Command('ping')
  commandPing(ctx: Context) {
    ctx.reply('pong');
  }

  @Command('send_hotfix_message')
  async commandSend_HotfixMessage(ctx: Context) {
    const adminId = this.configService.get('DEVELOPER_TELEGRAM_ID');
    const fromId = ctx?.message?.from?.id?.toString();

    if (!adminId || !fromId) {
      return;
    }

    if (adminId !== fromId) {
      return;
    }
    const rooms = await this.roomsService.getRooms();

    Promise.race(
      rooms.map(async (room) => {
        return await this.bot.telegram.sendMessage(
          room.chat,
          '✅ Были исправлены пожелания. Ваши ранее добавленные пожелания не были сохранены. Просьба написать свое пожелание повторно, через команду /wish мне в личном сообщении.\n\nИзвините за предоставленные неудобства :)',
        );
      }),
    );
  }

  @Command('admin_rooms_length')
  async commandAdminRoomsLength(ctx: Context) {
    const adminId = this.configService.get('DEVELOPER_TELEGRAM_ID');
    const fromId = ctx?.message?.from?.id?.toString();

    if (!adminId || !fromId) {
      return;
    }

    if (adminId !== fromId) {
      return;
    }

    const rooms = await this.roomsService.getRooms();
    ctx.reply(rooms?.length?.toString());
  }

  @Command('admin_users_length')
  async commandAdminUsersLength(ctx: Context) {
    const adminId = this.configService.get('DEVELOPER_TELEGRAM_ID');
    const fromId = ctx?.message?.from?.id?.toString();

    if (!adminId || !fromId) {
      return;
    }

    if (adminId !== fromId) {
      return;
    }

    const users = await this.usersService.getUsers();
    ctx.reply(users?.length?.toString());
  }

  @Command('songs')
  async commandSong(ctx: Context) {
    const songsIds = [
      'CQACAgIAAxkBAAIdL1_XdE_mkuyWAWxXY58DhQAB8agWOgACS_gDAAHx-W0GRPl4SCj1FKkeBA',
      'CQACAgIAAxkBAAIdMF_XdE9FoZBgEJB_j1WWUCi2blO4AAKABAACHZnoS3WjWLwcdQLbHgQ',
      'CQACAgQAAxkBAAIdMV_XdE9_FGtm3zW9kDFBfZEWQQ6EAAKxAAM9XrkF_komJEBhrtYeBA',
      'CQACAgIAAxkBAAIdMl_XdE8VbklK6fuvMJPuWnEoondkAAIiBQACQizBS4A6h-5rSwiDHgQ',
      'CQACAgIAAxkBAAIdM1_XdE9GElYvCauWaq39ce4crEbNAAJRBgACqdghSKxJbZfBOVyvHgQ',
      'CQACAgIAAxkBAAIdNF_XdE8GiDut9zzcTJlFoprM2xsLAAKoBAACOgMpS_VRvT0nHNzXHgQ',
      'CQACAgIAAxkBAAIdNV_XdE-F4O0R_Hi4jQ_kuoyr4H71AAJSBQACxvAJSv0lXbr-smOCHgQ',
      'CQACAgIAAxkBAAIdNl_XdE_NrwAB2EojdrZ5VEPdgAf2rgAC2AEAAlE28EhFqoHIaD6SIR4E',
      'CQACAgIAAxkBAAIdN1_XdE97CaqgPOP1nmO0LoW9fi4YAAIJBwACjQ2ZSGK67a2XbV9NHgQ',
    ];

    for (const songId of songsIds) {
      try {
        await ctx.replyWithAudio(songId);
        await delay(300);
      } catch (error) {}
    }
  }

  @Command('start')
  async commandStart(ctx: Context) {
    const roomId = ctx.message.text.split(' ')[1];
    if (!roomId) {
      try {
        await ctx.replyWithHTML(
          [
            '<b>Инструкция:</b>\n',
            '1. Добавить бота в группу',
            '2. Дать боту права администратора (Необязательный шаг)',
            '3. Вызвать команду /happy_new_year',
            '4. Дождаться всех участников',
            '5. Вызвать команду /ho_ho_ho',
          ].join('\n'),
        );
      } catch (error) {
        console.log('error', error);
      }
      return;
    }

    let id = null;

    try {
      id = Number(roomId);
    } catch (error) {}

    if (!id) {
      return;
    }

    const room = await this.roomsService.getRoomByChat(id);

    if (!room) {
      try {
        await ctx.reply('Я не смог найти ваш чат.');
      } catch (error) {}
      return;
    }

    if (room.isPlayed) {
      try {
        await ctx.reply('Игра уже началась.');
      } catch (error) {}
      return;
    }

    const existUser = await this.usersService.getUserByTelegram(ctx.from.id);

    if (existUser) {
      const userAlreadyHaveInRoom = room.users.some((id) =>
        id.equals(existUser._id),
      );
      if (userAlreadyHaveInRoom) {
        try {
          return await ctx.reply('Ты уже записан в списке участников.');
        } catch (error) {
          return error;
        }
      }
    }

    const user = await (async () => {
      if (existUser) {
        return existUser;
      }
      return await this.usersService.createUser({
        username: ctx.from.username,
        firstName: ctx.from.first_name,
        telegram: ctx.from.id,
      });
    })();

    await this.roomsService.addUserToRoom(room._id, user._id);

    await this.bot.telegram.sendMessage(
      room.chat,
      `Новый участник: <b>${
        user.firstName || user.username
      }</b> ✨\n<i>Количество участников: ${room.users.length + 1}</i>`,
      {
        parse_mode: 'HTML',
      },
    );

    try {
      this.bot.telegram
        .sendMessage(
          user.telegram,
          [
            `Теперь ты являешься участником игры "Тайный Санта" – ${room.title}.`,
            'По желанию можешь добавить своё пожелание /wish\n',
            'Внимание! Не выключай и не удаляй меня.',
            'Скоро я напишу тебе сообщение с именем человека для которого ты будешь Тайным Сантой.',
          ].join('\n'),
          {
            reply_markup: {
              inline_keyboard: [
                room.inviteLink
                  ? [
                      {
                        text: 'Перейти в группу',
                        url: room.inviteLink,
                      },
                    ]
                  : [],
              ],
            },
          },
        )
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {}
  }

  @Command('how_to_use')
  async commandHowToUse(ctx: Context) {
    try {
      await ctx.replyWithHTML(
        [
          '<b>Инструкция:</b>\n',
          '1. Добавить бота в группу',
          '2. Дать боту права администратора (Необязательный шаг)',
          '3. Вызвать команду /happy_new_year',
          '4. Дождаться всех участников',
          '5. Вызвать команду /ho_ho_ho',
        ].join('\n'),
      );
    } catch (error) {
      console.log('error', error);
    }
  }

  @Command('happy_new_year')
  async commandCreate(ctx: Context) {
    if (ctx.chat.type === 'private') {
      try {
        return await ctx.reply(
          'Только в групповых чатах можно вызвать эту команду',
        );
      } catch (error) {}
      return;
    }

    const existRoom = await this.roomsService.getRoomByChat(ctx.chat.id);
    if (existRoom) {
      try {
        return await ctx.reply('Вы уже создали игру в этом чате.');
      } catch (error) {}
      return;
    }

    const chat = await ctx.getChat();
    if (chat && chat.id) {
      await this.roomsService.createRoom({
        chat: chat.id,
        creator: ctx.from.id,
        title: chat.title || 'Безымянная группа',
      });
      try {
        const message = await ctx.replyWithHTML(
          [
            '<b>С наступающим Новым Годом!</b> ⭐️\n',
            'Давайте поиграем в Тайного Санту!',
            '<b>Для участия:</b>',
            '<b>1.</b> Нажмите на кнопку "Участвовать"',
            '<b>2.</b> После перехода нажмите на "Начать"',
          ].join('\n'),
          {
            reply_markup: {
              inline_keyboard: [
                [
                  {
                    text: 'Участвовать',
                    url: `${this.configService.get(
                      'TELEGRAM_BOT_SHARE_LINK',
                    )}?start=${ctx.chat.id}`,
                  },
                ],
              ],
            },
          },
        );
      } catch (error) {}
    }
  }

  @Command('help')
  async commandHelp(ctx: Context) {
    if (ctx.chat.type === 'private') {
      try {
        await ctx.replyWithHTML(
          [
            '<b>Мои команды:</b>\n',
            '/wish - Добавить пожелание',
            '/rooms - Мои группы где я участвую',
          ].join('\n'),
        );
      } catch (error) {}
    }

    if (ctx.chat.type === 'group') {
      try {
        await ctx.replyWithHTML(
          [
            '<b>Мои команды:</b>\n',
            '/happy_new_year - Зарегистрировать группу',
            '/ho_ho_ho - Запустить игру',
          ].join('\n'),
        );
      } catch (error) {}
    }
  }

  @Command('ho_ho_ho')
  async commandPlay(ctx: Context) {
    if (ctx.chat.type === 'private') {
      try {
        return await ctx.reply(
          'Только в групповых чатах можно вызвать эту команду',
        );
      } catch (error) {}
      return;
    }

    const room = await this.roomsService.getRoomByChat(ctx.chat.id);

    if (!room) {
      return await ctx.reply(
        'Для начала вам нужно пригласить участников. Воспользуйтесь командой /happy_new_year',
      );
    }

    if (room.isPlayed) {
      return await ctx.reply(
        'Вы не можете повторно разыграть группу. Вы можете начать игру заново командой /happy_new_year',
      );
    }

    if (room.users.length < 2) {
      return await ctx.reply(
        'Количество участников не должно быть меньше трех человек.',
      );
    }

    // const verifyUsers = await Promise.race(
    //   shuffledUsers.map(async (userId) => {
    //     const user = await this.usersService.getUserById(userId);
    //     this.bot.telegram.sendMessage(
    //       user.telegram,
    //       'Начинаю случайный поиск участника...',
    //     );
    //   }),
    // );

    const shuffledUsers = this.utilsService.shuffle(room.users);

    await Promise.race(
      shuffledUsers.map(async (id, index, array) => {
        const currentUser = await this.usersService.getUserById(id);

        const nextUser = await this.usersService.getUserById(
          array[index + 1] ?? array[0],
        );

        const wish = await this.wishesService.getWish(nextUser._id, room._id);

        try {
          await this.bot.telegram.sendMessage(
            currentUser.telegram,
            [
              `Ты даришь подарок: <b>${
                nextUser?.firstName ?? nextUser.username
              }</b>`,
              `Он участник группы: <b>${room.title}</b>\n`,
              'Пожелание к подарку:',
              `${wish?.text ?? 'Участник не добавил свое пожелаение.'}`,
            ].join('\n'),
            {
              parse_mode: 'HTML',
            },
          );
        } catch (error) {
          return await ctx.reply(
            `Не удалось запустить игру, потому что пользователь – ${currentUser.firstName} заблокировал меня.`,
          );
        }
      }),
    ).catch(() => {
      // TODO:
      return;
    });

    await this.roomsService.onPlayRoom(room._id);

    const HAPPY_NEW_YEAR_SONG_ID = 'CQADAgADBAUAAsU2sUvM6iJmiDVIuhYE';
    try {
      await ctx.replyWithAudio(HAPPY_NEW_YEAR_SONG_ID);
    } catch (error) {
      console.log(`Error: ${error.message}`);
    }

    try {
      await ctx.reply(
        [
          'Хо-хо-хо! 🎅🏻🎄🎉',
          'Я отправил каждому участнику сообщение с именем человека, которому он будет делать подарок.',
          'Проверьте ваши сообщения 🎁',
        ].join('\n'),
        {
          reply_markup: {
            inline_keyboard: [
              [
                {
                  text: 'Перейти в сообщения',
                  url: this.configService.get('TELEGRAM_BOT_SHARE_LINK'),
                },
              ],
            ],
          },
        },
      );
    } catch (error) {}
  }

  @Command('rooms')
  async commandRooms(ctx: Context) {
    if (ctx?.chat?.type !== 'private') {
      try {
        return await ctx.reply(
          'Только в личных сообщениях можно вызвать эту команду',
        );
      } catch (error) {}
      return;
    }

    const user = await this.usersService.getUserByTelegram(ctx.from.id);
    if (!user) {
      try {
        return await ctx.reply(
          [
            'Вы еще не играли в Тайного Санту',
            'Вы можете организовать свою игру /how_to_use',
          ].join('\n'),
        );
      } catch (error) {}
      return;
    }

    const rooms = await this.roomsService.getRoomsByUser(user._id);
    if (!rooms.length) {
      try {
        return await ctx.reply(
          [
            'У вас нет групп где вы участвуете',
            'Вы можете организовать свою игру /how_to_use',
          ].join('\n'),
        );
      } catch (error) {}
      return;
    }

    const names = rooms.map(
      (room) =>
        `<b>${room.title}</b> - ${
          room.isPlayed ? 'Разыграна 🎉' : 'Ожидает 🤔'
        }`,
    );

    try {
      await ctx.replyWithHTML(['<b>Твои группы:</b>\n', ...names].join('\n'));
    } catch (error) {}
  }

  @Command('wish')
  async commandWish(ctx: Context) {
    if (ctx.chat.type !== 'private') {
      try {
        return await ctx.reply(
          'Только в личных сообщениях можно вызвать эту команду',
        );
      } catch (error) {
        return;
      }
    }

    const user = await this.usersService.getUserByTelegram(ctx.from.id);
    if (!user) {
      try {
        return await ctx.reply(
          'Для добавления вишлиста нужно быть участником игры.',
        );
      } catch (error) {
        return;
      }
    }

    const rooms = await this.roomsService.getRoomsByUser(user._id);

    if (!rooms.length) {
      try {
        return await ctx.reply('Твои чаты не найдены.');
      } catch (error) {
        return;
      }
    }

    try {
      await ctx.reply(
        'Выбери чат в котором хочешь добавить или изменить свое пожелание:',
        {
          reply_markup: {
            inline_keyboard: [
              rooms.map((room) => ({
                text: room.title,
                callback_data: `wish_room~${room._id}`,
              })),
            ],
          },
        },
      );
    } catch (error) {}
  }
}
