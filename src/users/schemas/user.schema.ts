import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({
  versionKey: false,
  timestamps: true,
})
export class User extends Document {
  @Prop({
    required: false,
  })
  firstName: string;

  @Prop({
    required: false,
  })
  username?: string;

  @Prop({
    required: true,
  })
  telegram: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
