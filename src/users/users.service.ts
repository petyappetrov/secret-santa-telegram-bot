import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { CreateUserDTO } from './dto/create-user.dto';
import { User } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private usersModel: Model<User>) {}

  public async createUser(createUserDTO: CreateUserDTO): Promise<User> {
    return this.usersModel.create(createUserDTO);
  }

  public async getUserById(id: Types.ObjectId): Promise<User> {
    return this.usersModel.findById(id);
  }

  public async getUserByTelegram(telegram: number): Promise<User> {
    return this.usersModel.findOne({ telegram });
  }

  public async getUsers(): Promise<User[]> {
    return this.usersModel.find().exec();
  }
}
