import { Injectable } from '@nestjs/common';

@Injectable()
export class UtilsService {
  shuffle<T>(array: T[]): T[] {
    const arr = [...array];
    let counter = arr.length;

    while (counter > 0) {
      const index = Math.floor(Math.random() * counter);

      counter--;

      const temp = arr[counter];
      arr[counter] = arr[index];
      arr[index] = temp;
    }

    return arr;
  }

  getRandom<T>(array: T[]): T {
    return array[Math.floor(Math.random() * array.length)];
  }
}
