import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import { RoomsModule } from './rooms/rooms.module';
import { TelegramBotModule } from './telegram-bot/telegram-bot.module';
import { UsersModule } from './users/users.module';
import { UtilsService } from './utils.service';
import { WishesModule } from './wishes/wishes.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.local', '.env'],
    }),
    TelegramBotModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const host = configService.get('DB_HOST');
        const port = configService.get('DB_PORT');
        const name = configService.get('DB_NAME');
        const username = configService.get('DB_USERNAME');
        const password = configService.get('DB_PASSWORD');
        return {
          uri: `mongodb://${username}:${password}@${host}:${port}/${name}`,
          useCreateIndex: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useNewUrlParser: true,
        };
      },
    }),
    UsersModule,
    RoomsModule,
    WishesModule,
  ],
  providers: [UtilsService],
})
export class AppModule {}
