import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from 'src/users/schemas/user.schema';

@Schema({
  versionKey: false,
  timestamps: true,
})
export class Room extends Document {
  @Prop({
    required: true,
  })
  creator: number;

  @Prop({
    required: true,
  })
  chat: number;

  @Prop({
    required: true,
  })
  title: string;

  @Prop({
    required: false,
    type: [
      {
        type: Types.ObjectId,
        ref: User,
      },
    ],
  })
  users?: Types.ObjectId[];

  @Prop({
    required: false,
  })
  inviteLink?: string;

  @Prop({
    required: false,
    default: false,
  })
  isPlayed?: boolean = false;
}

export const RoomSchema = SchemaFactory.createForClass(Room);
