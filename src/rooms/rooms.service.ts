import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { CreateRoomDTO } from './dto/create-room.dto';
import { Room } from './schemas/room.schema';

@Injectable()
export class RoomsService {
  constructor(
    @InjectModel(Room.name) private readonly roomModel: Model<Room>,
  ) {}

  public createRoom(createRoomDTO: CreateRoomDTO): Promise<Room> {
    return this.roomModel.create(createRoomDTO);
  }

  public addUserToRoom(
    roomId: Types.ObjectId,
    userId: Types.ObjectId,
  ): Promise<Room> {
    return this.roomModel
      .findByIdAndUpdate(
        roomId,
        { $addToSet: { users: userId } },
        { new: true },
      )
      .exec();
  }

  public async getRoomByChat(chat: number): Promise<Room> {
    return this.roomModel.findOne({ chat });
  }

  public async getRooms(): Promise<Room[]> {
    return this.roomModel.find().exec();
  }

  public async getRoomsByUser(userId: Types.ObjectId): Promise<Room[]> {
    return this.roomModel.find({ users: { $in: [userId] } });
  }

  public async getActiveRoomsByUser(userId: Types.ObjectId): Promise<Room[]> {
    return this.roomModel.find({ users: { $in: [userId] }, isPlayed: false });
  }

  public async onPlayRoom(roomId: Types.ObjectId): Promise<Room> {
    return this.roomModel.findByIdAndUpdate(
      roomId,
      { isPlayed: true },
      { new: true },
    );
  }
}
