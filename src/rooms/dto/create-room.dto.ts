import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateRoomDTO {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsNumber()
  chat: number;

  @IsNotEmpty()
  @IsNumber()
  creator: number;

  @IsOptional()
  @IsBoolean()
  isPlayed?: boolean;
}
